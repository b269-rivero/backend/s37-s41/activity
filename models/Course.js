// const mongoose = require("mongoose");

// const userSchema = new mongoose.Schema(
// {
// 	name : {
// 		type : String,
// 		required : [true, "Course name is required"]
// 	},
// 	description : {
// 		type : String,
// 		required : [true, "Course description is required"]
// 	},
// 	price : {
// 		type : Number,
// 		required : [true, "Price is required"]
// 	},
// 	isActive : {
// 		type : String,
// 		required : [true, "Course no longer active"]
// 	},
// 	isAdmin : {
// 		type : Boolean,
// 		required : [true, "Only students are eligible"] 
// 	},
	
// 	enrollees : [
// 		{
// 			userId : {
// 				type : String,
// 				required : [true, "User ID is required"]
// 			},
// 			enrolledOn : {
// 				type : Date,
// 				default : new Date()
// 				required : [true, "Date of enrollment required"]
// 			}
// 	]
// });

// module.exports = mongoose.model("Course", courseSchema);

const mongoose = require("mongoose");

const courseSchema = new mongoose.Schema({
	name : {
		type : String,
		required : [true, "Course is required"]
	},
	description : {
		type : String,
		required : [true, "Description is required"]
	},
	price : {
		type : Number,
		required : [true, "Price is required"]
	},
	isActive : {
		type : Boolean,
		default : true
	},
	createdOn : {
		type : Date,
		default : new Date()
	},
	enrollees : [
		{
			userId : {
				type : String,
				required: [true, "UserId is required"]
			},
			enrolledOn : {
				type : Date,
				default : new Date() 
			}
		}
	]
});

module.exports = mongoose.model("Course", courseSchema);
